# Rails On Docker

## Dependencies
You need to have Docker installed on your machine to run the project locally.

- **Ubuntu** https://docs.docker.com/install/linux/docker-ce/ubuntu/
- **MacOS** https://docs.docker.com/docker-for-mac/install/

## Installation (development)
```
make build
make start
```

Once started, application can be browsed at http://localhost:3000.