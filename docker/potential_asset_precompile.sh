#! /bin/sh

echo 'Running potential asset precompile...'

if [ $# -eq 0 ] ; then
  echo 'No argument supplied, so precompiling...'
  RAILS_ENV=production bundle exec rake assets:precompile
else
  if [ $1 == "NO" ] ; then
    echo "Not precompiling assets..."
  else
    echo 'Precompiling assets...'
    RAILS_ENV=production bundle exec rake assets:precompile
  fi
fi
