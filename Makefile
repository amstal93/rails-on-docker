.PHONY: build
## Build the project on development mode
build:
	docker-compose build
	@echo
	@echo "$$(tput bold)$$(tput setaf 6)> Project build complete (development)$$(tput sgr0)"
	@echo

.PHONY: start
## Start the application in development mode
start:
	docker-compose up

.PHONY: console
## Run the Rails console inside the app container
console:
	docker exec -it rails-on-docker_app_1 bin/rails console

.PHONY: shell
## Access the app container in shell mode
shell:
	docker exec -it rails-on-docker_app_1 sh

.PHONY: clear
## Clear the application
clear:
	docker-compose down


.DEFAULT_GOAL := help
.PHONY: help
help:
	@echo "Usage: make <COMMAND>"
	@echo
	@echo "Commands:"
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
	-v ncol=$$(tput cols) \
	-v indent=12 \
	-v col_on="$$(tput setaf 6)" \
	-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s ", " "; \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}'
	@echo
